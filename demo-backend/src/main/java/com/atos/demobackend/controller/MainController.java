package com.atos.demobackend.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.atos.demobackend.entities.Response;

@RestController
@RequestMapping("/api")
public class MainController {
	
	@GetMapping("/validate")
	public ResponseEntity<Response> validatePds(@RequestHeader("X-artemis-pds") String pds){
		if(!pds.matches("[0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12}")) 
			return new ResponseEntity<>(new Response("KO"), HttpStatus.OK);
		return new ResponseEntity<>(new Response("OK"), HttpStatus.OK);
	}
}
