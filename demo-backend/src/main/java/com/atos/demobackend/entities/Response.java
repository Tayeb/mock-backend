package com.atos.demobackend.entities;

public class Response {
	private String validation;
	
	public Response(String validation) {
		this.validation = validation;
	}

	public String getValidation() {
		return validation;
	}

	public void setValidation(String validation) {
		this.validation = validation;
	}
	
	
}
